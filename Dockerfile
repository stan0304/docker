FROM docker:stable-dind
MAINTAINER  stan.peyssard@gmail.com

COPY dind.sh /usr/local/bin/

ENTRYPOINT ["dind.sh"]
CMD []