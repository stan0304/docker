# Docker in Docker stable image allowing arguments

A stable [Docker-in-Docker](https://hub.docker.com/_/docker/) image allowing arguments. 

```yaml
image: docker:stable-git

services:
    - stan0304/docker:stable-dind
      command: "--insecure-registry=my.docker.registry"
```

